package de.sebastianhesse.examples.spring;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import static de.sebastianhesse.examples.spring.SpringDynamoDBLocalTest.dynamoDBLocal;


/**
 * A simple Spring test config providing the DynamoDB client as a Spring bean.
 */
public class SpringTestConfig {

    private static final Logger logger = LoggerFactory.getLogger(SpringTestConfig.class);
    private static final String DEFAULT_REGION = "us-east-1";

    @Bean
    public AmazonDynamoDB dynamoDbClient() {
        return createAmazonDynamoDBClient(buildEndpointUrl());
    }

    @NotNull
    private String buildEndpointUrl() {
        String serviceEndpoint = "http://localhost:";

        if (dynamoDBLocal.isRunning()) {
            serviceEndpoint += dynamoDBLocal.getFirstMappedPort();
        } else {
            throw new IllegalStateException("DynamoDB Local is not running.");
        }

        logger.info("Using DynamoDB endpoint: {}", serviceEndpoint);

        return serviceEndpoint;
    }


    /**
     * Creates a DynamoDB client which connects to the given {@code serviceEndpoint}.
     *
     * @param serviceEndpoint
     *         service endpoint URL where DynamoDB Local is running
     * @return a DynamoDB client
     */
    private AmazonDynamoDB createAmazonDynamoDBClient(String serviceEndpoint) {
        AwsClientBuilder.EndpointConfiguration endpointConfiguration =
                new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, DEFAULT_REGION);

        return AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(endpointConfiguration)
                .build();
    }

}
