package de.sebastianhesse.examples.springTwo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * A sample class showing how to write a test using a preconfigured DynamoDB local instance from a parent class.
 * The DynamoDB table is reset for each test class, see notes in {@link AbstractSpringDynamoDBLocalTest}.
 */
public class SpringDynamoDBLocalTestTwo extends AbstractSpringDynamoDBLocalTest {

    private static final Logger logger = LoggerFactory.getLogger(SpringDynamoDBLocalTestTwo.class);

    @Autowired
    private AmazonDynamoDB client;


    @Test
    public void connectionSuccessful() {
        String tableHashKey = "id";
        String tableName = "test-table";

        CreateTableResult table = createCreateDynamoDBTable(client, tableName, tableHashKey);

        logger.info("Created table: {}", table.getTableDescription());
    }

}
