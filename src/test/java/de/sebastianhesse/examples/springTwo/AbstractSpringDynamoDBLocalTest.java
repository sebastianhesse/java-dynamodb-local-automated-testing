package de.sebastianhesse.examples.springTwo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testcontainers.containers.GenericContainer;

import java.util.ArrayList;
import java.util.List;


/**
 * An abstract test class for running tests with Spring. DynamoDB Local is started before the tests run.
 * Important note: This class is used as a base class for test classes {@link SpringDynamoDBLocalTestOne} and
 * {@link SpringDynamoDBLocalTestTwo}. Thus, the DynamoDB Local container is re-started due to {@link ClassRule}. As a result, the
 * container will get a new port under which DynamoDB Local can be reached. In order to update the Spring bean of the DynamoDB
 * client, we're using {@link DirtiesContext} to refresh the Spring context for each test class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringTestConfig.class})
// see notes above why this annotation is necessary
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public abstract class AbstractSpringDynamoDBLocalTest {

    // This declaration will start a DynamoDB Local Docker container -> make sure to specify the exposed port as 8000, otherwise the port
    // mapping will be wrong -> see the docs: https://hub.docker.com/r/amazon/dynamodb-local
    @ClassRule
    public static GenericContainer dynamoDBLocal = new GenericContainer("amazon/dynamodb-local:1.11.477")
            .withExposedPorts(8000);


    /**
     * Creates a DynamoDB table with one hash key.
     *
     * @param client
     *         client to access DynamoDB
     * @param tableName
     *         a table name
     * @param tableHashKey
     *         a name for the hash key attribute
     * @return result of the table create request
     */
    protected CreateTableResult createCreateDynamoDBTable(AmazonDynamoDB client, String tableName, String tableHashKey) {
        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition(tableHashKey, ScalarAttributeType.S));

        List<KeySchemaElement> ks = new ArrayList<>();
        ks.add(new KeySchemaElement(tableHashKey, KeyType.HASH));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput(10L, 10L);

        CreateTableRequest request = new CreateTableRequest()
                .withTableName(tableName)
                .withAttributeDefinitions(attributeDefinitions)
                .withKeySchema(ks)
                .withProvisionedThroughput(provisionedthroughput);

        return client.createTable(request);
    }

}
