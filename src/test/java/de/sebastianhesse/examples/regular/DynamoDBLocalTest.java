package de.sebastianhesse.examples.regular;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import org.jetbrains.annotations.NotNull;
import org.junit.ClassRule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;

import java.util.ArrayList;
import java.util.List;


/**
 * A simply unit test to show how you can use DynamoDB Local with Testcontainers.org.
 * See further examples using Spring in {@link de.sebastianhesse.examples.spring} and {@link de.sebastianhesse.examples.springTwo}.
 */
public class DynamoDBLocalTest {

    private static final Logger logger = LoggerFactory.getLogger(DynamoDBLocalTest.class);
    private static final String DEFAULT_REGION = "us-east-1";

    // This declaration will start a DynamoDB Local Docker container -> make sure to specify the exposed port as 8000, otherwise the port
    // mapping will be wrong -> see the docs: https://hub.docker.com/r/amazon/dynamodb-local
    @ClassRule
    public static GenericContainer dynamoDBLocal = new GenericContainer("amazon/dynamodb-local:1.11.477")
            .withExposedPorts(8000);


    @Test
    public void connectionSuccessful() {
        String tableHashKey = "id";
        String tableName = "test-table";

        String serviceEndpoint = buildEndpointUrl();
        AmazonDynamoDB client = createAmazonDynamoDBClient(serviceEndpoint);
        CreateTableResult table = createCreateDynamoDBTable(client, tableName, tableHashKey);

        logger.info("Created table: {}", table.getTableDescription());
    }


    /**
     * Builds the endpoint url where DynamoDB Local is running under.
     *
     * @return a url to DynamoDB Local
     */
    @NotNull
    private String buildEndpointUrl() {
        String serviceEndpoint = "http://localhost:";

        if (dynamoDBLocal.isRunning()) {
            serviceEndpoint += dynamoDBLocal.getFirstMappedPort();
        } else {
            throw new IllegalStateException("DynamoDB Local is not running.");
        }

        logger.info("Using DynamoDB endpoint: {}", serviceEndpoint);

        return serviceEndpoint;
    }


    /**
     * Creates a DynamoDB client which connects to the given {@code serviceEndpoint}.
     *
     * @param serviceEndpoint
     *         service endpoint URL where DynamoDB Local is running
     * @return a DynamoDB client
     */
    private AmazonDynamoDB createAmazonDynamoDBClient(String serviceEndpoint) {
        AwsClientBuilder.EndpointConfiguration endpointConfiguration =
                new AwsClientBuilder.EndpointConfiguration(serviceEndpoint, DEFAULT_REGION);

        return AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(endpointConfiguration)
                .build();
    }


    /**
     * Creates a DynamoDB table with one hash key.
     *
     * @param client
     *         client to access DynamoDB
     * @param tableName
     *         a table name
     * @param tableHashKey
     *         a name for the hash key attribute
     * @return result of the table create request
     */
    private CreateTableResult createCreateDynamoDBTable(AmazonDynamoDB client, String tableName, String tableHashKey) {
        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition(tableHashKey, ScalarAttributeType.S));

        List<KeySchemaElement> ks = new ArrayList<>();
        ks.add(new KeySchemaElement(tableHashKey, KeyType.HASH));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput(10L, 10L);

        CreateTableRequest request = new CreateTableRequest()
                .withTableName(tableName)
                .withAttributeDefinitions(attributeDefinitions)
                .withKeySchema(ks)
                .withProvisionedThroughput(provisionedthroughput);

        return client.createTable(request);
    }
}
