# java-dynamodb-local-automated-testing

This repository provides examples for Java how to use [DynamoDB Local](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html) in automated tests using [Testcontainers](https://www.testcontainers.org/).
Tests are executed using [JUnit 4](https://junit.org/junit4/) and also provide examples how to integrate all with [Spring](https://spring.io/).
Additionally, it provides configuration to properly run these tests in [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines).

## Quick Start

Checkout the project and run the Maven tests locally:

```bash
$ git clone git@bitbucket.org:sebastianhesse/java-dynamodb-local-automated-testing.git
$ mvn clean compile test
```

You can find all the examples in the package [de.sebastianhesse.examples](./src/test/java/de/sebastianhesse/examples).

## Requirements

- A Bitbucket/Atlassian Account
- Docker installed on your system
- AWS configured: either [on the CLI using a fake profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) or by providing env variables


## Further Links

- [DynamoDB Local](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)
- [JUnit 4](https://junit.org/junit4/)
- [Testcontainers](https://www.testcontainers.org/)
- [Spring](https://spring.io/)
- [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) 